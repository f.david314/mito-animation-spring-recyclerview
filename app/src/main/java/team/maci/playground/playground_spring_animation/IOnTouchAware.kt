package team.maci.playground.playground_spring_animation

import android.view.MotionEvent

interface IOnTouchAware{
    fun onTouchEvent(event: MotionEvent?)
}