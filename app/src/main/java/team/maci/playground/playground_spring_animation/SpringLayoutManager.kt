package team.maci.playground.playground_spring_animation

import android.graphics.PointF
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber

class SpringLayoutManager(
    val recyclerView: RecyclerView
) : RecyclerView.LayoutManager(), RecyclerView.SmoothScroller.ScrollVectorProvider, IOnTouchAware {
    val animationEndSubject = BehaviorSubject.createDefault(false)

    var firstItemIndex: Int = 0
    var lastItemIndex: Int = 0

    private var currentSelectedIndex = 0

    private var firstItemTopPosition = 0
    private var lastItemBottomPosition = 0

    private var transformationContainer = TransformationContainer()

    private val normalizeAnimator : NormalizeAnimator = NormalizeAnimator(transformationContainer, this).apply {
        animationEndListener = {
            Timber.d("Animation end listener triggered")
            animationEndSubject.onNext(true)
        }
    }

    companion object {
        val DIRECTION_BOTTOM = 0
        val DIRECTION_UP = 1
    }

    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
        return RecyclerView.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
        super.onLayoutChildren(recycler, state)

        detachAndScrapAttachedViews(recycler)

        if (itemCount == 0) {
            return
        }

        if (firstItemIndex > itemCount) {
            firstItemIndex = 0
        }

        lastItemIndex = firstItemIndex
        firstItemTopPosition = 0
        lastItemBottomPosition = 0

        currentSelectedIndex = 0

        do {
            addView(recycler, DIRECTION_BOTTOM)
        } while (lastItemBottomPosition < verticalSpace && !isLastItemReached)

        if (lastItemBottomPosition < verticalSpace && firstItemIndex > 0) {
            do {
                addView(recycler, DIRECTION_UP)
                val measuredHeight = getDecoratedMeasuredHeight(getChildAt(0)!!)
                val transformationAmount = Math.min(measuredHeight, verticalSpace - lastItemBottomPosition)
                allChilren {
                    it.offsetTopAndBottom(transformationAmount)
                }
                lastItemBottomPosition += transformationAmount
                firstItemTopPosition += transformationAmount
            } while (firstItemIndex > 0 && lastItemBottomPosition < verticalSpace)
        }
    }

    override fun onAdapterChanged(oldAdapter: RecyclerView.Adapter<*>?, newAdapter: RecyclerView.Adapter<*>?) {
        super.onAdapterChanged(oldAdapter, newAdapter)
        removeAllViews()
    }

    fun addView(recycler: RecyclerView.Recycler, direction: Int, addSpringSpace: Boolean = false): Boolean {
        if (direction != DIRECTION_UP && direction != DIRECTION_BOTTOM) {
            throw IllegalArgumentException("Illegal direction parameter: $direction")
        }

        if (direction == DIRECTION_UP && firstItemIndex == 0 || direction == DIRECTION_BOTTOM && lastItemIndex == itemCount) {
            return false
        }

        val position = if (direction == DIRECTION_BOTTOM) lastItemIndex++ else --firstItemIndex
        val targetView = recycler.getViewForPosition(position)
        if (direction == DIRECTION_BOTTOM) {
            addView(targetView)
        } else {
            addView(targetView, 0)
        }

        measureChild(targetView, 0, 0)

        val measuredWidth = getDecoratedMeasuredWidth(targetView)
        val measuredHeight = getDecoratedMeasuredHeight(targetView)

        Timber.d("Measured width: $measuredWidth, height: $measuredHeight")

        val springSpace = if(addSpringSpace) 50 else 0

        layoutDecorated(
            targetView,
            0,
            if (direction == DIRECTION_BOTTOM) lastItemBottomPosition + springSpace else firstItemTopPosition - measuredHeight - springSpace,
            measuredWidth,
            if (direction == DIRECTION_BOTTOM) lastItemBottomPosition + measuredHeight + springSpace else firstItemTopPosition - springSpace
        )

        if (direction == DIRECTION_BOTTOM) {
            lastItemBottomPosition += measuredHeight + springSpace
        } else {
            firstItemTopPosition -= measuredHeight + springSpace
        }

        return true
    }

    override fun scrollToPosition(position: Int) {
        super.scrollToPosition(position)
        if (position in 0 until itemCount) {
            firstItemIndex = position
            requestLayout()
        } else {
            Timber.w("Invalid scroll position")
        }
    }

    fun removeView(recycler: RecyclerView.Recycler, direction: Int): Boolean {
        if (direction != DIRECTION_UP && direction != DIRECTION_BOTTOM) {
            throw IllegalArgumentException("Illegal direction argument")
        }


        if (direction == DIRECTION_UP) {
            removeAndRecycleView(getChildAt(0) ?: return false, recycler)
            firstItemIndex++
            val newFirstChild = getChildAt(0)
            firstItemTopPosition = if (newFirstChild != null) getDecoratedTop(newFirstChild) else 0

        } else {
            removeAndRecycleView(getChildAt(childCount - 1) ?: return false, recycler)
            lastItemIndex--
            val newLastChild = getChildAt(childCount - 1)
            lastItemBottomPosition = if (newLastChild != null) getDecoratedBottom(newLastChild) else 0
        }
        return true
    }


    override fun canScrollVertically(): Boolean {
        return true
    }

    override fun scrollVerticallyBy(dyOriginal: Int, recycler: RecyclerView.Recycler, state: RecyclerView.State): Int {
        if (!hasEnoughItemsToScroll) {
            return 0
        }


        val currentSelectedIndex = getCurrentSelectedIndex()

        Timber.d("Current selected index: $currentSelectedIndex")

        handleItemAddition(recycler, dyOriginal)
        transformationContainer.init(childCount)
        var dy = dyOriginal
        Timber.d("Modified dy (original(): $dy $lastItemIndex $lastItemBottomPosition $verticalSpace")

        if(dy < 0 && firstItemIndex == 0){
            var availableScrollableSpace = 0
            for(i in currentSelectedIndex downTo 0){
                val currentChild = getChildAt(i) !!
                val prevChild = getChildAt(i - 1)
                val prevBottom = if(prevChild != null) getDecoratedBottom(prevChild) else 0
                val currentTop = getDecoratedTop(currentChild)

                availableScrollableSpace += 50 - (currentTop - prevBottom)
            }
            Timber.d("Available scrollable space: $availableScrollableSpace, dy: $dy")

            dy = Math.max(dy, -availableScrollableSpace)
        }

        if(dy > 0 && lastItemIndex == itemCount){
            var availableScrollableSpace = 0
            for(i in currentSelectedIndex until childCount){
                val currentChild = getChildAt(i) !!
                val nextChild = getChildAt(i + 1)
                val currentBottom = getDecoratedBottom(currentChild)
                val nextTop = if(nextChild != null) getDecoratedTop(nextChild) else verticalSpace

                availableScrollableSpace += 50 - (nextTop - currentBottom)
            }
            Timber.d("Available scrollable space: $availableScrollableSpace, dy: $dy")

            dy = Math.min(dy, availableScrollableSpace)
        }

        var availableScroll = -dy

        for (i in currentSelectedIndex downTo 0) {
            val target = getChildAt(i) ?: break
            val prevBottom = if (i > 0) getDecoratedBottom(getChildAt(i - 1)!!) else 0

            if (dy < 0) {
                transformationContainer[i] += availableScroll

                val currentChildTop = getDecoratedTop(target) + availableScroll
                Timber.d("Prev decorated bottom: $prevBottom, currentChildTop: $currentChildTop")
                availableScroll = -Math.min(0, 50 - (currentChildTop - prevBottom))
                Timber.d("Available scroll: $availableScroll")
                if (availableScroll <= 0) {
                    Timber.d("Exit because available scroll is small")
                    break
                }
            } else {
                //Scroll up
                transformationContainer[i] += availableScroll
                val currentTop = getDecoratedTop(target) + availableScroll
                availableScroll = (currentTop - prevBottom)

                if (availableScroll >= 0) {
                    break
                }
            }


        }

        transformationContainer[currentSelectedIndex] = 0

        availableScroll = -dy
        for (i in currentSelectedIndex until childCount) {
            val target = getChildAt(i) ?: break
            val nextTop =
                if (i + 1 < childCount) getDecoratedTop(getChildAt(i + 1)!!) else verticalSpace

            if (dy > 0) {
                //Scroll up
                transformationContainer[i] += availableScroll
                val currentBottom = getDecoratedBottom(target) + availableScroll
                Timber.d("Prev decorated bottom: $nextTop, currentChildTop: $currentBottom")
                availableScroll = -Math.max(0, (nextTop - currentBottom) - 50)
                Timber.d("Available scroll: $availableScroll")

                if (availableScroll >= 0) {
                    break
                }
            } else {
                transformationContainer[i] += availableScroll
                val currentBottom = getDecoratedBottom(target) + availableScroll
                availableScroll = (currentBottom - nextTop)

                if (availableScroll <= 0) {
                    break
                }
            }
        }

        for (i in 0 until transformationContainer.size) {
            val target = getChildAt(i)
            val transformAmount = transformationContainer[i]
            if (target != null && transformAmount != 0) {
                target.offsetTopAndBottom(transformAmount)
            }
        }

        val lastItem = getChildAt(childCount - 1)
        if(lastItem != null){
            lastItemBottomPosition = getDecoratedBottom(lastItem)
        }

        val firstItem = getChildAt(0)
        if(firstItem != null){

            firstItemTopPosition = getDecoratedTop(firstItem)
        }

        Timber.d("Transformation when scrolling: ${transformationContainer.debug()}")


        var topGapSum = 0
        var bottomGapSum = 0
        for(i in (currentSelectedIndex - firstItemIndex) downTo 1){
            val target = getChildAt(i) ?: continue
            val prevTarget = getChildAt(i - 1) ?: continue
            topGapSum += getDecoratedTop(target) - getDecoratedBottom(prevTarget)
        }

        for(i in (currentSelectedIndex - firstItemIndex) until childCount - 1){
            val target = getChildAt(i) ?: continue
            val nextTarget = getChildAt(i + 1) ?: continue
            bottomGapSum += getDecoratedTop(nextTarget) - getDecoratedBottom(target)
        }

        handleItemAddition(recycler, -(topGapSum + bottomGapSum), (topGapSum + bottomGapSum))

        handleItemRemove(recycler, -(topGapSum + bottomGapSum), (topGapSum + bottomGapSum))

        Timber.d("First item top: Scroll")


        return dy
    }



    fun handleItemAddition(recycler: RecyclerView.Recycler, topAdditionalSpace: Int = 0, bottomAdditionalSpace: Int = topAdditionalSpace){
        val extendedBottomPosition = -50 + verticalSpace + bottomAdditionalSpace
        var lastChildBottom : Int
        do{
            val lastChild = getChildAt(childCount - 1) ?: break
            lastChildBottom = getDecoratedBottom(lastChild)

            if(lastChildBottom < extendedBottomPosition){
                val result = addView(recycler, DIRECTION_BOTTOM)
                if(!result){
                    break
                }
            }
        }while(lastChildBottom < extendedBottomPosition)



        val extendedTopPosition = 50 + topAdditionalSpace
        var firstChildTop : Int
        do{
            val firstChild = getChildAt(0) ?: break
            firstChildTop = getDecoratedTop(firstChild)
            if(firstChildTop > extendedTopPosition){
                val result = addView(recycler, DIRECTION_UP, true)
                Timber.d("Item add to top, firstChildTop: $firstChildTop, extendedTopPosition: $extendedTopPosition, childCount: $childCount, result: $result")
                if(!result){
                    break
                }
            }
        }while(firstChildTop > extendedTopPosition)

    }

    fun handleItemRemove(recycler: RecyclerView.Recycler, topAdditionalSpace: Int = 0, bottomAdditionalSpace: Int= 0) {
        var lastChildTop : Int
        val extendedVerticalSpace = verticalSpace + bottomAdditionalSpace
        do{
            val lastChild = getChildAt(childCount - 1) ?: break
            lastChildTop = getDecoratedTop(lastChild)

            if(lastChildTop > extendedVerticalSpace){
                val result = removeView(recycler, DIRECTION_BOTTOM)
                if(!result){
                    break
                }
            }
        }while(lastChildTop > extendedVerticalSpace)

        var firstChildBottom : Int
        val extendedTopPosition = topAdditionalSpace
        do{
            val firstItem = getChildAt(0) ?: break
            firstChildBottom = getDecoratedBottom(firstItem)
            if(firstChildBottom < extendedTopPosition){
                val result = removeView(recycler, DIRECTION_UP)
                Timber.d("Item remove from top")
                if(!result){
                    break
                }
            }
        }while(firstChildBottom < extendedTopPosition)
    }




    override fun onScrollStateChanged(state: Int) {
        val stateString = when(state){
            RecyclerView.SCROLL_STATE_DRAGGING -> "SCROLL_STATE_DRAGGING"
            RecyclerView.SCROLL_STATE_IDLE -> "SCROLL_STATE_IDLE"
            RecyclerView.SCROLL_STATE_SETTLING -> "SCROLL_STATE_SETTLING"
            else -> "Unknown"
        }
        Timber.d("Scroll state changed: $stateString")
        if (RecyclerView.SCROLL_STATE_IDLE == state || RecyclerView.SCROLL_STATE_SETTLING == state) {
            normalizeAnimator.start()
        }else{
            //normalizeAnimator.cancel()
        }
        super.onScrollStateChanged(state)
    }

    override fun smoothScrollToPosition(
        recyclerView: RecyclerView, state: RecyclerView.State,
        position: Int
    ) {

        if (position in 0 until itemCount) {
            val linearSmoothScroller = object: LinearSmoothScroller(recyclerView.context){
                override fun getVerticalSnapPreference(): Int {
                    return SNAP_TO_START
                }
            }

            linearSmoothScroller.targetPosition = position
            startSmoothScroll(linearSmoothScroller)
        } else {
            Timber.e("Invalid position to smooth scroll: $position")
        }
    }

    override fun computeScrollVectorForPosition(targetPosition: Int): PointF? {
        currentSelectedIndex = targetPosition
        val targetDirection = if(targetPosition < firstItemIndex) -1f else 1f
        return PointF(0f, targetDirection)
    }

    inline fun allChilren(block: (View) -> Unit) {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            if (child != null) {
                block(child)
            }
        }
    }


    fun getCurrentSelectedIndex(): Int {
        Timber.d("Current selected index was before then first item: $currentSelectedIndex")
        val normalizedCurrentSelectedIndex = currentSelectedIndex - firstItemIndex
        return when {
            normalizedCurrentSelectedIndex < 0 -> 0
            normalizedCurrentSelectedIndex >= childCount -> childCount - 1
            else -> normalizedCurrentSelectedIndex
        }
    }

    override fun onTouchEvent(event: MotionEvent?) {
        if(event?.action == MotionEvent.ACTION_DOWN){
            val y = event.y
            var targetIndex = 0
            var child = getChildAt(targetIndex)!!
            while(targetIndex < childCount && getDecoratedTop(child) < y){
                val childTop = getDecoratedTop(child)
                val childBottom = getDecoratedBottom(child)
                if(childTop <= y && childBottom >= y){
                    break
                }
                targetIndex++
                val nextChild = getChildAt(targetIndex) ?: break
                child = nextChild
            }
            currentSelectedIndex = targetIndex + firstItemIndex
        }
    }

    val isLastItemReached: Boolean
        get() = lastItemIndex >= itemCount

    val hasEnoughItemsToScroll: Boolean
        get() {
            if (firstItemIndex == 0 && lastItemIndex == itemCount) {
                val firstChildDecoratedTop = getDecoratedTop(getChildAt(0)!!)
                val lastChildDecoratedButton = getDecoratedBottom(getChildAt(childCount - 1)!!)

                return lastChildDecoratedButton - firstChildDecoratedTop > verticalSpace
            }
            return true
        }

    val verticalSpace: Int
        get() = height - paddingTop - paddingBottom


}