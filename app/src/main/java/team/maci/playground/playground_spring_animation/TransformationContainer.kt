package team.maci.playground.playground_spring_animation



class TransformationContainer{
    private var transformationArray = IntArray(5)
    private var transformationArraySize = transformationArray.size
    private var containsNonZeroItem = false

    val size : Int
        get() = transformationArraySize

    fun clear(){
        for(i in 0 until transformationArray.size){
            transformationArray[i] = 0
        }
        containsNonZeroItem = false
    }

    fun init(requestedSize: Int){
        if(requestedSize > transformationArray.size){
            transformationArray = IntArray(requestedSize)
        }
        transformationArraySize = requestedSize
        clear()
    }


    operator fun get(index: Int) : Int{
        return transformationArray[index]
    }

    operator fun set(index: Int, value: Int){
        transformationArray[index] = value
        if(value != 0){
            containsNonZeroItem = true
        }
    }

    fun hasAnyTransformation() = containsNonZeroItem

    fun currentMaximumArrayNumber() = transformationArray.size

    fun debug() : String{
        val stringBuilder = StringBuilder()
        for(i in 0 until transformationArraySize){
            if(i != 0){
                stringBuilder.append(" ")
            }
            stringBuilder.append(transformationArray[i].toString().padEnd(3))
        }
        return stringBuilder.toString()
    }
}