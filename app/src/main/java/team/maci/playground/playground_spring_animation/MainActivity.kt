package team.maci.playground.playground_spring_animation

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.util.concurrent.TimeUnit


class ExampleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
class ExampleAdapter : RecyclerView.Adapter<ExampleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        return ExampleViewHolder(
            FrameLayout(parent.context).apply {
                TextView(parent.context).also {
                    it.layoutParams = ViewGroup.MarginLayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        150
                    ).apply {
                        setMargins(20, 10, 20, 10)
                    }
                    it.gravity = Gravity.CENTER
                    it.setBackgroundColor(Color.parseColor("#33b5e5"))
                    addView(it)
                }
            })
    }

    override fun getItemCount(): Int {
        return 200
    }

    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
        val viewGroup = holder.itemView as ViewGroup
        val textView = viewGroup.getChildAt(0) as TextView
        textView.text = position.toString()
    }
}

class MainActivity : AppCompatActivity() {

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Timber.plant(Timber.DebugTree())

        var layoutManager = SpringLayoutManager(recyclerView)

        recyclerView.adapter = ExampleAdapter()
        recyclerView.layoutManager = layoutManager

//        Observable.timer(1, TimeUnit.SECONDS)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe {
//                recyclerView.smoothScrollToPosition(18)
//            }
//        var currentSelectedIndex = 0
//
//
//
//        layoutManager
//            .animationEndSubject
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .delay(3, TimeUnit.SECONDS)
//            .subscribe {
//                recyclerView.smoothScrollToPosition(++currentSelectedIndex)
//            }
    }
}
