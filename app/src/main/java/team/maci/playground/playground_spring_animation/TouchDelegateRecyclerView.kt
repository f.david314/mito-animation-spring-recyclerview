package team.maci.playground.playground_spring_animation

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView

//TODO: Rename
class TouchDelegateRecyclerView : RecyclerView{
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun onTouchEvent(e: MotionEvent?): Boolean {
        val layoutManager = layoutManager
        if(layoutManager is IOnTouchAware){
            layoutManager.onTouchEvent(e)
        }
        return super.onTouchEvent(e)
    }
}