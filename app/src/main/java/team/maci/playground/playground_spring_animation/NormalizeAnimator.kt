package team.maci.playground.playground_spring_animation

import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import timber.log.Timber
import java.util.concurrent.TimeUnit

class NormalizeAnimator(
    private val transformationContainer: TransformationContainer,
    private val layoutManager: SpringLayoutManager
) {
    private var currentSpeed = START_SPEED
    private var disposable: Disposable? = null
    private var cancelled = false
    var currentScrollSpeed = 0

    var animationEndListener: (() -> Unit)? = null

    fun reset() {
        currentSpeed = START_SPEED
    }

    fun start() {
        if (disposable != null && disposable?.isDisposed != true) {
            return
        }
        cancelled = false

        val subscriber = object : DisposableSubscriber<Long>() {
            override fun onComplete() {
                //Ignored
            }

            override fun onStart() {
                super.onStart()
                request(1)
            }

            override fun onNext(t: Long?) {
                onTimeUpdate()
                request(1)
            }

            override fun onError(t: Throwable?) {
                Timber.e(t)
                disposable = null
            }
        }
        disposable = subscriber

        Flowable
            .interval(1000 / 60, TimeUnit.MILLISECONDS)
            .onBackpressureDrop()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(subscriber)
    }

    fun cancel() {
        cancelled = true
        disposable?.dispose()
    }

    fun onTimeUpdate() {
        if (cancelled) {
            return
        }
        //currentSpeed += 0.0001f

        transformationContainer.init(layoutManager.childCount)

        var currentSelectedIndex = layoutManager.getCurrentSelectedIndex()

        if (layoutManager.recyclerView.scrollState != RecyclerView.SCROLL_STATE_DRAGGING
            || layoutManager.getDecoratedTop(layoutManager.getChildAt(layoutManager.childCount - 1)!!) > layoutManager.verticalSpace - 50
        ) {
            var bottomAvailableTransformation = BOTTOM_AVAIALBLE_TRANSFORMATION
            for (i in currentSelectedIndex + 1 until layoutManager.childCount) {
                val target = layoutManager.getChildAt(i) ?: continue
                val prevTarget = layoutManager.getChildAt(i - 1) ?: continue
                val prevBottom = layoutManager.getDecoratedBottom(prevTarget) + transformationContainer[i - 1]
                val currentTop = layoutManager.getDecoratedTop(target) + transformationContainer[i]
                val actualTransformation = Math.min(currentTop - prevBottom, currentSpeed.toInt())
                bottomAvailableTransformation -= actualTransformation

                transformationContainer[i] += -actualTransformation
                if (bottomAvailableTransformation <= 0) {
                    break
                }
            }


            var additionalSpringEffect = 0
            for (i in currentSelectedIndex + 1 until layoutManager.childCount) {
                val target = layoutManager.getChildAt(i) ?: continue
                val prevTarget = layoutManager.getChildAt(i - 1) ?: continue
                val prevTargetBottom = layoutManager.getDecoratedBottom(prevTarget) + transformationContainer[i - 1]
                val targetTop = layoutManager.getDecoratedTop(target) + transformationContainer[i]
                val transformAmount = (targetTop - prevTargetBottom) - 50 + additionalSpringEffect / 4
                if (transformAmount > 0) {
                    additionalSpringEffect = transformAmount
                    transformationContainer[i] += -transformAmount
                }
            }
        }


        if (layoutManager.recyclerView.scrollState != RecyclerView.SCROLL_STATE_DRAGGING
            || layoutManager.getDecoratedTop(layoutManager.getChildAt(0)!!) < 50
        ) {

            Timber.d("FTT: ----------")
            var topAvaibleTransformation = TOP_AVAILABLE_TRANSFORMATION
            for (i in currentSelectedIndex - 1 downTo 0) {
                val target = layoutManager.getChildAt(i) ?: continue
                val nextTarget = layoutManager.getChildAt(i + 1) ?: continue
                val nextTop = layoutManager.getDecoratedTop(nextTarget) + transformationContainer[i + 1]
                val currentBottom = layoutManager.getDecoratedBottom(target) + transformationContainer[i]

                val transformAmount = Math.min(nextTop - currentBottom, currentSpeed.toInt())
                topAvaibleTransformation -= transformAmount
                transformationContainer[i] += transformAmount
                if (topAvaibleTransformation <= 0) {
                    break
                }
            }

            Timber.d("FTT: 1 - " + transformationContainer.debug())

            for (i in currentSelectedIndex - 1 downTo 0) {
                val target = layoutManager.getChildAt(i) ?: continue
                val nextTarget = layoutManager.getChildAt(i + 1) ?: continue

                val nextTargetTop = layoutManager.getDecoratedTop(nextTarget) + transformationContainer[i + 1]
                val targetBottom = layoutManager.getDecoratedBottom(target) + transformationContainer[i]
                val transformAmount = (nextTargetTop - targetBottom) - 50
                if (transformAmount > 0) {
                    transformationContainer[i] += transformAmount
                }
            }

            Timber.d("FTT: 2 - " + transformationContainer.debug())
        }

        if (layoutManager.recyclerView.scrollState != RecyclerView.SCROLL_STATE_DRAGGING) {
            layoutManager.getChildAt(0)?.let { firstItem ->
                val firstItemTop = layoutManager.getDecoratedTop(firstItem)
                val firstTransformation = transformationContainer[0]
                Timber.d("FTT: First item top: $firstItemTop")
                if (layoutManager.firstItemIndex == 0 && firstItemTop >= 0) {
                    for (i in 0 until transformationContainer.size) {
                        //Normalize first item
                        transformationContainer[i] -= Math.min(currentSpeed.toInt(), firstItemTop)

                        //Normalize current item if the gap is greater than GAP limit
//                    if (i > 0) {
//                        val currentView = layoutManager.getChildAt(i) ?: continue
//                        val prevView = layoutManager.getChildAt(i - 1) ?: continue
//
//                        val prevViewBottom = layoutManager.getDecoratedBottom(prevView) + transformationContainer[i - 1]
//                        val currentViewTop = layoutManager.getDecoratedTop(currentView) + transformationContainer[i]
//
//                        val distanceDiff = Math.max((currentViewTop - prevViewBottom) - 50, 0)
//
//                        transformationContainer[i] -= distanceDiff
//
//                    }
                    }

                }
                Timber.d("FTT: 3 - " + transformationContainer.debug())

            }
        }


        if (layoutManager.recyclerView.scrollState != RecyclerView.SCROLL_STATE_DRAGGING) {
            val lastItem = layoutManager.getChildAt(layoutManager.childCount - 1)
            if (lastItem != null) {
                val lastItemBottom = layoutManager.getDecoratedBottom(lastItem)
                val lastItemTransformation = transformationContainer[layoutManager.childCount - 1]
                if (layoutManager.lastItemIndex == layoutManager.itemCount && lastItemBottom <= layoutManager.verticalSpace) {
                    for (i in 0 until transformationContainer.size) {
                        //transformationContainer[i] -= lastItemTransformation

                        transformationContainer[i] += Math.min(
                            layoutManager.verticalSpace - lastItemBottom,
                            currentSpeed.toInt()
                        )
                    }
                }
            }
        }

        for (i in 0 until transformationContainer.size) {
            val target = layoutManager.getChildAt(i) ?: continue
            val actualTransformation = transformationContainer[i]
            if (actualTransformation != 0) {
                target.offsetTopAndBottom(actualTransformation)
            }
        }


        if (!transformationContainer.hasAnyTransformation()) {
            //cancelled = true
            //disposable?.dispose()
            //disposable = null
            //animationEndListener?.invoke()
        }
    }

    companion object {
        private const val START_SPEED = 15.0
        private const val BOTTOM_AVAIALBLE_TRANSFORMATION = 20
        private const val TOP_AVAILABLE_TRANSFORMATION = 20
    }
}